# @havlasme/react-toolkit

[![npm version][npm-version-image]][npm-version-link]
[![npm download][npm-download-image]][npm-download-link]
[![Apache-2.0 license][license-image]][license-link]
[![semantic release][semantic-release-image]][semantic-release-link]
[![pipeline status][pipeline-image]][pipeline-link]
[![coverage report][coverage-image]][pipeline-link]

React ToolKit.

## installation

```
npm install @havlasme/react-toolkit
```

```
yarn add @havlasme/react-toolkit
```

## development

```
yarn run dev
```

## storybook

```
yarn run storybook
```

[npm-version-image]: https://img.shields.io/npm/v/@havlasme/react-toolkit.svg?style=flat-square
[npm-version-link]: https://npmjs.org/package/@havlasme/react-toolkit
[npm-download-image]: https://img.shields.io/npm/dm/@havlasme/react-toolkit.svg?style=flat-square
[npm-download-link]: https://npmcharts.com/compare/@havlasme/react-toolkit?minimal=true
[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: LICENSE
[semantic-release-image]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?style=flat-square
[semantic-release-link]: https://github.com/semantic-release/semantic-release
[pipeline-image]: https://img.shields.io/gitlab/pipeline-status/havlas.me/react-toolkit?branch=main&style=flat-square
[pipeline-link]: https://gitlab.com/havlas.me/react-toolkit/-/pipelines?ref=main
[coverage-image]: https://img.shields.io/gitlab/pipeline-coverage/havlas.me/react-toolkit?branch=main&style=flat-square
