import {createContext} from 'react'

// the cache context.
const CacheContext = createContext(null)

export default CacheContext
