import {HookTestBedControl} from './index'

const hookTestBedControl = function (TestBed) {
  return (
    <HookTestBedControl>
      <TestBed/>
    </HookTestBedControl>
  )
}

export default hookTestBedControl
