const UseExternalTestBed = function () {
  return (
    <div className="space-y-4">
      <div className="space-y-2">
        @see <strong>useScript</strong><br/>
        @see <strong>useStyleSheet</strong>
      </div>
    </div>
  )
}

export default UseExternalTestBed
